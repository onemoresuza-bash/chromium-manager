DOCKER_ENV_DIR = tests/docker
DOCKER_DIR = docker
DOCKER_TAG = cmcontainer

docker-all:
	@printf "Available Targets:\n\n"
	@printf "\033[1mdocker-compose-up:\033[m"
	@printf "\033[1mdocker-compose-down:\033[m"

docker-setup:
	mkdir -p $(DOCKER_ENV_DIR)
	cp -rv $(DOCKER_DIR)/* $(DOCKER_ENV_DIR)
	cp -rv $(SRC)/* $(DOCKER_ENV_DIR)

docker-compose-up: docker-setup
	cd $(DOCKER_ENV_DIR); \
		docker compose up

docker-compose-down:
	cd $(DOCKER_ENV_DIR); \
		docker compose down --remove-orphans --rmi local
	rm -rfvd $(DOCKER_ENV_DIR)

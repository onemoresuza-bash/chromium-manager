#!/usr/bin/env bash
# file: src/tests/help.bash

#
# Shellcheck Global Directives
#
# Suppress warnings about not being able to follow files:
# shellcheck source=/dev/null
#

oneTimeSetUp() {
  unset HELP_MSG
}

testHelpCmdAliases() {
  for alias in "help" "-h"; do
    if [ -z "${HELP_MSG}" ]; then
      HELP_MSG="$(bash chromium-manager "${alias}")"
    else
      bash chromium-manager "${alias}" 1>/dev/null
    fi

    assertTrue " Alias \"${alias}\" did not work" "${?}"
  done

  return 0
}

testHelpMsgContents() {
  for cmd in "create" "launch" "remove" "rm" "list" "ls" "help" "-h"; do
    assertContains "${HELP_MSG}" "${cmd}"
  done

  return 0
}

source shunit2

# vim: set tw=100 ts=2 sw=2 et:
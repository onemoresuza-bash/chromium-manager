#!/usr/bin/env bash
# file: src/tests/help.bash

#
# Shellcheck Global Directives
#
# Suppress warnings about not being able to follow files:
# shellcheck source=/dev/null
#

oneTimeSetUp() {
  PROFILES_DIR="/prof-dir"
  PROFILE_NAME="prof1"
}

tearDown() {
  rm -rf "${PROFILES_DIR}"
}

testCreateOptionP() {
  bash chromium-manager -p "${PROFILES_DIR}" create "${PROFILE_NAME}"
  assertTrue "Failed to create profile \"${PROFILE_NAME}\"" "${?}"

  bash chromium-manager -p "${PROFILES_DIR}" create -f "${PROFILE_NAME}"
  assertTrue "Failed to create profile \"${PROFILE_NAME}\"" "${?}"

  bash chromium-manager -p "${PROFILES_DIR}" create "${PROFILE_NAME}" 2>&1
  assertFalse "Succeed to create profile \"${PROFILE_NAME}\"" "${?}"

  return 0
}

source shunit2

# vim: set tw=100 ts=2 sw=2 et:
#!/usr/bin/env bash
# file: src/tests/global-opts.bash

#
# Shellcheck Global Directives
#
# Suppress warnings about not being able to follow files:
# shellcheck source=/dev/null
#
# Suppress warnings about permissions of last folder for mkdir:
# shellcheck disable=SC2174
#

oneTimeSetUp() {
  P_OPT_TEST_DIR="/success-test-dir"
  mkdir -pm0700 "${P_OPT_TEST_DIR}"
}

testOptionP() {
  bash chromium-manager -p "${P_OPT_TEST_DIR}" help 1>/dev/null
  assertTrue "Failed to create profile dir\"${P_OPT_TEST_DIR}\"; perm: 0700" \
    "${?}"

  return 0
}

source shunit2

# vim: set tw=100 ts=2 sw=2 et:
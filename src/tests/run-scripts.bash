#!/usr/bin/env bash

for s in "tests/scripts/"*; do
  bash "${s}"
done

# vim: set tw=100 ts=2 sw=2 et: